package logica;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2023-08-14T19:12:00")
@StaticMetamodel(Consumible.class)
public class Consumible_ { 

    public static volatile SingularAttribute<Consumible, String> marcaConsumible;
    public static volatile SingularAttribute<Consumible, Integer> id_consumible;
    public static volatile SingularAttribute<Consumible, String> tipoConsumible;
    public static volatile SingularAttribute<Consumible, Integer> cantidadConsumible;

}