package logica;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.7.v20200504-rNA", date="2023-08-14T19:12:00")
@StaticMetamodel(Equipo.class)
public class Equipo_ { 

    public static volatile SingularAttribute<Equipo, String> marcaEquipo;
    public static volatile SingularAttribute<Equipo, String> tipo;
    public static volatile SingularAttribute<Equipo, String> serie;
    public static volatile SingularAttribute<Equipo, Integer> id_equipo;
    public static volatile SingularAttribute<Equipo, String> modelo;

}