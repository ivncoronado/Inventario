# Inventario
Sistema de Control de Consumibles y Equipos (Inventario)

 Descripción del Proyecto

Este proyecto tiene como objetivo desarrollar un sistema de control eficiente de consumibles y equipos de cómputo. El sistema permitirá un seguimiento preciso de los recursos, generará alertas por reabastecimiento y proporcionará reportes detallados para una gestión más efectiva.

Características

- Registro de consumibles y equipos.
- Registro de entradas y salidas con fecha y departamento.
- Generación de alertas por reabastecimiento.
- Reportes de stock disponible y de entrada de insumos y equipos.

Tecnologías Utilizadas

- JDK 11 
- IDE Neatbeans
- Wampserver para la base de datos con Mysql
- Java EE (Servlets, JSP) para el desarrollo web.
- JPA para la persistencia de datos.
- Base de datos MySQL para el almacenamiento de información.

Instalación y Uso

1. Clona este repositorio.
2. Configura la conexión a la base de datos en el archivo de configuración.
3. Ejecuta el servidor web y accede a la aplicación desde tu navegador.
4. Registra consumibles, equipos, entradas y salidas según sea necesario.
5. Visualiza reportes de stock y entradas para tomar mejores decisiones.



Contribuciones

Las contribuciones son bienvenidas. Si deseas colaborar, sigue los pasos:

1. Haz un fork de este repositorio.
2. Crea una rama con una descripción clara de tu contribución.
3. Realiza tus cambios y haz commits.
4. Envía un pull request explicando tus cambios y su propósito.

Autor

-Iván Coronado https://github.com/ivncoronado/Inventario.git  Desarrollador Principal
